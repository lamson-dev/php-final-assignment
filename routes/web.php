<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', 'DataFeededController@index');

Route::prefix('datafeed')->group(function(){
    Route::post('storeData',[
        'as' => 'store',
        'uses' => 'DataFeededController@store'
    ]);

    Route::put('updateData/{id}',[
        'as' => 'update',
        'uses' => 'DataFeededController@update'
    ]);
    Route::get('editData/{id}',[
        'as' => 'edit',
        'uses' => 'DataFeededController@edit'
    ]);

    Route::delete('deleteData/{id}',[
        'as' => 'delete',
        'uses' => 'DataFeededController@destroy'
    ]);
    Route::get('deleteData/{id}',[
        'as' => 'delete',
        'uses' => 'DataFeededController@destroy'
    ]);
});