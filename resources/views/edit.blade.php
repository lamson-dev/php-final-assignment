<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>edit</title>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css" />
</head>
<body><div class="container" style="padding:10px 10px;">

<h2>Feeded XML Data</h2>
<div class="well clearfix">
    <!-- <fieldset class="scheduler-border"> -->
    <h4 class="scheduler-border">Add Item</h4>
    <form method="POST" action="{{Route('update', $dataFeeded->id)}}" class="form-inline">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputName">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$dataFeeded->title}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputName">Description</label>
            <input type="text" class="form-control" id="description" name="description" value="{{$dataFeeded->description}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputName">Link</label>
            <input type="text" class="form-control" id="link" name="link" value="{{$dataFeeded->link}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputName">Category</label>
            <input type="text" class="form-control" id="category" name="category" value="{{$dataFeeded->category}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputName">Comments</label>
            <input type="text" class="form-control" id="comments" name="comments" value="{{$dataFeeded->comments}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="form-group">
            <label for="exampleInputName">Publish Date</label>
            <input type="text" class="form-control" id="pubDate" name="pubDate" value="{{$dataFeeded->pubDate}}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>Update Data</button>
    </form>
    <!-- </fieldset> -->
</div>
</div>
    
</body>
</html>