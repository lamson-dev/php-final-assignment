<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataFeeded extends Model
{
    //
    protected $table = 'data_feededs';
    protected $primayKey = 'id';

    protected $fillable = [
        'title','description','link','category','comments','pubDate'
    ];
}
