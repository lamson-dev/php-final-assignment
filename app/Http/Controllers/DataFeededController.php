<?php

namespace App\Http\Controllers;

use App\DataFeeded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DataFeededController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataFeeded = DataFeeded::paginate(5);
        return view('index', ['dataFeededRes'=>$dataFeeded]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'category' => 'required',
            'comments' => 'required',
            'pubDate' => 'required',
        ]);

        DataFeeded::create($request->all());

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataFeeded = DataFeeded::find($id);
        return view('edit',['dataFeeded' => $dataFeeded]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'link' => 'required',
            'category' => 'required',
            'comments' => 'required',
            'pubDate' => 'required',
        ]);
        $dataFeeded = DataFeeded::find($id);

        $dataFeeded->title = $request->input('title');
        $dataFeeded->description = $request->input('description');
        $dataFeeded->link = $request->input('link');
        $dataFeeded->category = $request->input('category');
        $dataFeeded->comments = $request->input('comments');
        $dataFeeded->pubDate = $request->input('pubDate');

        $dataFeeded->save();
        // return redirect('test1')->with('success','Great! Product updated successfully');
        return redirect('/');
     }
        

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DataFeeded::destroy($id);
        return redirect('/');
    }
}
