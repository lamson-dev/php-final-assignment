<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InsertDataEasy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:InsertDataEasy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand use to insert data to database easily ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('data_feededs')->insert([
            'title' => Str::random(10).'_title_random',
            'description' => Str::random(50).'_description_random',
            'link' => 'http://'.Str::random(15).'com/',
            'category' => 'Category_'.Str::random(25),
            'comments' => 'http://'.Str::random(15).'com/',
            'pubDate' => 'http://'.Str::random(15).'com/',
        ]);
        $this->info('Inserted new random data into database');
    }
}
/* run this command on console cmd/ terminal
 php artisan insert:InsertDataEasy*/
